﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpawnerConfig : ScriptableObject 
{
    public static SpawnerConfig Instance;
    public Shape[] shapeView;
    public Color32[] colors;

}
