﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShapeType
{
    Cube,
    Sphere,
    Cylinder
}

public class PoolManager : MonoBehaviour 
{
    [System.Serializable]
    public class Pool
    {
        public ShapeType type;
        public Shape prefab;
        public int size;
    }

    public static PoolManager Instance;

    public SpawnerConfig config;
    public int poolSize = 10;

    public List<Pool> pools;
    public Dictionary<ShapeType, Queue<Shape>> poolDictionary;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }
        if (Instance != this)
        {
            Destroy(this);
        }
    }

    void Start()
    {
        poolDictionary = new Dictionary<ShapeType, Queue<Shape>>();
        pools.Clear();

        for (int i = 0; i < System.Enum.GetValues(typeof(ShapeType)).Length; i++)
        {
            pools.Add(new Pool { type = (ShapeType)i, prefab = config.shapeView[i], size = poolSize });
        }

        foreach (var pool in pools)
        {
            Queue<Shape> objectPool = new Queue<Shape>();

            for (int i = 0; i < pool.size; i++)
            {
                Shape s = Instantiate(pool.prefab);
                s.gameObject.SetActive(false);
                objectPool.Enqueue(s);
            }

            poolDictionary.Add(pool.type, objectPool);
        }
    }

    public Shape SpawnFromPool(ShapeType type)
    {
        Shape shape = poolDictionary[type].Dequeue();
        shape.gameObject.SetActive(true);
        var randomColorIndex = Random.Range(0, config.colors.Length);
        shape.color = config.colors[randomColorIndex];

        poolDictionary[type].Enqueue(shape);

        return shape;
    }
}


