﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour 
{
    public Transform startPoint;
    public Transform endPoint;
    public float spawnTime;

    int shapeTypeCount;
    float nextTimeStep = 10f;
    float currentMovespeed = 2f;

    void Start()
    {
        shapeTypeCount = System.Enum.GetValues(typeof(ShapeType)).Length;
        StartCoroutine(Spawn());
    }

    void Update()
    {
        if (Time.timeSinceLevelLoad > nextTimeStep)
        {
            nextTimeStep += 10f;
            currentMovespeed += 0.2f * currentMovespeed;
        }
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);
            var shape = PoolManager.Instance.SpawnFromPool((ShapeType)Random.Range(0, shapeTypeCount));
            shape.transform.position = startPoint.transform.position;
            shape.transform.rotation = Quaternion.identity;
            shape.ResetToInitial();
            shape.MoveTo(endPoint, currentMovespeed);
        }
    }
}
