﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;
using System;

public class UITrackableEventHandler : DefaultTrackableEventHandler 
{
    public Text startText;
    public UnityEvent OnIntroPlayed;

    bool introPlayed;

    protected override void OnTrackingFound()
    {
        if (!introPlayed)
        {
            startText.gameObject.SetActive(true);
            StartCoroutine("PlayIntro");
        }
    }

    protected override void OnTrackingLost()
    {
        StopCoroutine("PlayIntro");
        startText.gameObject.SetActive(false);
    }

    IEnumerator PlayIntro()
    {
        startText.text = "3";
        yield return new WaitForSeconds(1);
        startText.text = "2";
        yield return new WaitForSeconds(1);
        startText.text = "1";
        yield return new WaitForSeconds(1);
        startText.text = "Start!";
        startText.gameObject.SetActive(false);
        OnIntroPlayed.Invoke();
        introPlayed = true;
    }
}
