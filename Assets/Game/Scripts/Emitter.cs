﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Emitter : MonoBehaviour 
{
    public ParticleSystem particle;

    ParticleSystemRenderer psr;
    int minParticleCount = 30;
    int maxParticleCount = 50;

    void Start()
    {
        particle = GetComponentInChildren<ParticleSystem>();
        psr = particle.GetComponent<Renderer>().GetComponent<ParticleSystemRenderer>();
        psr.mesh = GetComponent<MeshFilter>().mesh;
    }

    public void Emit()
    {
        psr.material.color = GetComponent<Renderer>().material.color;
        particle.Emit(Random.Range(minParticleCount, maxParticleCount + 1));
    }
}
