﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class GameManager : MonoBehaviour
{
    public Text scoreText;

    bool introPlayed;
    int score;

    public void StartGame()
    {
        introPlayed = true;
        scoreText.gameObject.SetActive(true);
        scoreText.text = score.ToString();
    }

    void Start()
    {
        scoreText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && introPlayed)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                var target = hit.collider.gameObject.GetComponent<Shape>();
                if (target != null && !target.emissionTriggered)
                {
                    target.Disappear(true);
                    score++;
                    scoreText.text = score.ToString();
                }
            }
        }
    }
}
