﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour 
{
    public Color32 color;

    public bool emissionTriggered;

    float rotationSpeed = 50f;
    Emitter emitter;
    MeshRenderer rend;

    void Awake()
    {
        rend = GetComponent<MeshRenderer>();
        emitter = GetComponent<Emitter>();
    }

    public void ResetToInitial()
    {
        rend.enabled = true;
        rend.material.color = color;
        emissionTriggered = false;
    }

    public void MoveTo(Transform target, float speed)
    {
        StartCoroutine(Move(target, speed, Random.insideUnitSphere));
    }

    IEnumerator Move(Transform target, float speed, Vector3 rotation)
    {
        while (Vector3.Distance(transform.position, target.position) > 1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * speed);
            transform.Rotate(rotation, rotationSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        Disappear(false);
    }

    public void Disappear(bool triggerEmission)
    {
        if (!emissionTriggered)
        {
            StartCoroutine(DisappearCoroutine(triggerEmission));
        }
        emissionTriggered = true;
    }

    IEnumerator DisappearCoroutine(bool triggerEmission)
    {
        if (triggerEmission)
        {
            rend.enabled = false;
            emitter.Emit();
            yield return new WaitForSeconds(3f);
        }
        gameObject.SetActive(false);
    }
}
